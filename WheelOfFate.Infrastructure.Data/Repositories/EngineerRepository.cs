﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WheelOfFate.Domain.Entities;
using WheelOfFate.Domain.Repositories;
using WheelOfFate.Infrastructure.Data.Context;
using System.Data.Entity;

namespace WheelOfFate.Infrastructure.Data.Repositories
{
    public class EngineerRepository : IEngineerRepository
    {

        private readonly WheelOfFateContext _context;

        public EngineerRepository(WheelOfFateContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<Engineer>> GetAvailableEngineersWithLastShiftBeforeDate(Team team, DateTime start)
        {
            return await _context.Engineers
                .Where(e => e.TeamId == team.Id)
                .Where(e => !e.Shifts.Any() || e.Shifts.Select(s => s.Date).DefaultIfEmpty().Max() < start)
                .ToArrayAsync();
        }

    }
}
