﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WheelOfFate.Domain.Entities;
using WheelOfFate.Infrastructure.Data.Migrations;

namespace WheelOfFate.Infrastructure.Data.Context
{
    public class WheelOfFateContext: IdentityDbContext<User>
    {
        public DbSet<Engineer> Engineers { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Team> Teams { get; set; }

        public WheelOfFateContext(): base("WheelOfFate")
        {
            System.Data.Entity.Database.SetInitializer(new WheelOfFateInitializer());
        }

        public static IdentityDbContext<User> Create()
        {
            return new WheelOfFateContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            
            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("nvarchar"));
            modelBuilder.Properties<string>().Configure(p => p.HasMaxLength(100));

        }

    }
}
