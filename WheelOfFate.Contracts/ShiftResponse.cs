﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WheelOfFate.Contracts
{
    public class ShiftResponse
    {

        public string EngineerName { get; set; }
        
        public DateTime ShiftDate { get; set; }

        public int ShiftOrder { get; set; }
        
    }
}
