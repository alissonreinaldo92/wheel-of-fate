﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WheelOfFate.Contracts;
using WheelOfFate.Infrastructure.Data.Context;
using System.Data.Entity;

namespace WheelOfFate.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {

        [Route("{id}")]
        public async Task<IHttpActionResult> GetById(string id)
        {
            using (var db = new WheelOfFateContext())
            {
                var user = await db.Users.Where(u => u.Id == id).Select(u => new ApplicationUser
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email
                }).FirstOrDefaultAsync();

                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [Route("by-email/{email}")]
        public async Task<IHttpActionResult> GetByEmail(string email)
        {
            using (var db = new WheelOfFateContext())
            {
                var user = await db.Users.Where(u => u.Email == email).Select(u => new ApplicationUser
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email
                }).FirstOrDefaultAsync();

                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }

        [Route("by-name/{userName}")]
        public async Task<IHttpActionResult> GetByName(string userName)
        {
            using (var db = new WheelOfFateContext())
            {
                var user = await db.Users.Where(u => u.UserName == userName).Select(u => new ApplicationUser
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Email = u.Email
                }).FirstOrDefaultAsync();

                if (user == null)
                    return NotFound();
                return Ok(user);
            }
        }
 
    }
}
