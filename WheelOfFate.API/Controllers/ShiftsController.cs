﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WheelOfFate.Contracts;
using WheelOfFate.Infrastructure.Data.Context;
using System.Data.Entity;

namespace WheelOfFate.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/shifts")]
    public class ShiftsController : ApiController
    {

        [Route("by-team-from-date/{teamId}/{quantity:int}/{from:datetime}")]
        public async Task<IHttpActionResult> GetShiftsByTeamFromDate(string teamId, int quantity, DateTime from)
        {
            using (var db = new WheelOfFateContext())
            {
                var shifts = await db.Shifts.Where(s => s.TeamId == teamId && s.Date >= from).Select(s => new ShiftResponse
                {
                    EngineerName = s.Engineer.Name,
                    ShiftDate = s.Date,
                    ShiftOrder = s.Order
                })
                .OrderBy(s => s.ShiftDate)
                .ThenBy(s => s.ShiftOrder)
                .Take(quantity)
                .ToArrayAsync();
                
                return Ok(shifts);
            }
        }

    }
}