﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using FluentScheduler;
using WheelOfFate.API.FluentScheduler;

[assembly: OwinStartup(typeof(WheelOfFate.API.Startup))]

namespace WheelOfFate.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            SeedDatabase();

        }
    }
}
