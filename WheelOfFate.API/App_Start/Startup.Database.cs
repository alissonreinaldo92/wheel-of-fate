﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Linq;
using WheelOfFate.API.App_Start;
using WheelOfFate.Domain.Entities;
using WheelOfFate.Infrastructure.Data.Context;

namespace WheelOfFate.API
{
    public partial class Startup
    {
        
        public void SeedDatabase()
        {
            // Add Admin User
            using (var context = new WheelOfFateContext())
            {
                if (!context.Users.Any())
                {
                    var userManager = new WheelOfFateUserManager(new UserStore<User>(context));

                    var user = new User
                    {
                        Email = "admin@wheeloffate.com",
                        UserName = "Admin",
                        EmailConfirmed = true
                    };

                    userManager.Create(user, "Admin");
                }
            }
        }

    }
}