﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using WheelOfFate.API.App_Start;
using WheelOfFate.API.Providers;
using WheelOfFate.Domain.Entities;
using WheelOfFate.Infrastructure.Data.Context;

namespace WheelOfFate.API
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }
        
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(WheelOfFateContext.Create);
            app.CreatePerOwinContext<WheelOfFateUserManager>(Startup.CreateUserManager);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

        }

        public static WheelOfFateUserManager CreateUserManager(IdentityFactoryOptions<WheelOfFateUserManager> options, IOwinContext context)
        {
            var manager = new WheelOfFateUserManager(new UserStore<User>(new WheelOfFateContext()));
            if (options != null)
                manager.ConfigureTokenProvider(options.DataProtectionProvider);
            return manager;
        }

    }
}
