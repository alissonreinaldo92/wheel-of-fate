﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WheelOfFate.API.Client;
using System.Threading.Tasks;

namespace WheelOfFate.API.Tests
{
    [TestClass]
    public class UsersApiTests
    {

        [TestMethod]
        public async Task GetToken()
        {
            WheelOfFateApiClient client = new WheelOfFateApiClient();
            var token = await client.GetToken("Admin", "Admin");
            Assert.IsNotNull(token.access_token);
        }

        [TestMethod]
        public async Task GetUserByEmail()
        {
            WheelOfFateApiClient client = new WheelOfFateApiClient();
            var token = await client.GetToken("Admin", "Admin");
            var user = await client.GetUserByEmail("admin@wheeloffate.com", token.access_token);
            Assert.IsNotNull(user.UserName);
        }

        [TestMethod]
        public async Task GetUserById()
        {
            WheelOfFateApiClient client = new WheelOfFateApiClient();
            var token = await client.GetToken("Admin", "Admin");
            var user = await client.GetUserById("41c619f6-9288-4e5f-a8f6-6567ab106688", token.access_token);
            Assert.IsNotNull(user.UserName);
        }

        [TestMethod]
        public async Task GetUserByName()
        {
            WheelOfFateApiClient client = new WheelOfFateApiClient();
            var token = await client.GetToken("Admin", "Admin");
            var user = await client.GetUserByName("Admin", token.access_token);
            Assert.IsNotNull(user.UserName);
        }

        [TestMethod]
        public async Task GetShiftsByTeamFromDate()
        {
            WheelOfFateApiClient client = new WheelOfFateApiClient();
            var token = await client.GetToken("Admin", "Admin");
            var shifts = await client.GetShiftsByTeamFromDate("3fc6e49b-38f2-4bcb-b6ef-9d4a5538ba73", 10, DateTime.Today, token.access_token);
            Assert.IsTrue(shifts.Any());
        }

    }
}
