﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using WheelOfFate.Contracts;

namespace WheelOfFate.Presentation.App_Start.IdentityConfig
{

    public class WheelOfFateStore :
        //IUserPasswordStore<ApplicationUser>,
        //IUserEmailStore<ApplicationUser>,
        //IUserLockoutStore<ApplicationUser, string>,
        IUserStore<ApplicationUser>
    {

        public Task CreateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            var requestUserToken = HttpContext.Current.Items["user-token"].ToString();
            var user = await WheelOfFateApplication.apiClient.GetUserByEmail(email, requestUserToken);
            return user;
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId)
        {
            var requestUserToken = HttpContext.Current.Items["user-token"].ToString();
            var user = await WheelOfFateApplication.apiClient.GetUserById(userId, requestUserToken);
            return user;
        }

        public async Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var requestUserToken = HttpContext.Current.Items["user-token"].ToString();
            var user = await WheelOfFateApplication.apiClient.GetUserByName(userName, requestUserToken);
            return user;
        }

        public Task<int> GetAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetEmailAsync(ApplicationUser user)
        {
            return Task.FromResult(user.Email);
        }

        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> GetLockoutEnabledAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
        
        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> IncrementAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
        
        public Task ResetAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAsync(ApplicationUser user, string email)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            throw new NotImplementedException();
        }
        
        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }
    }
}