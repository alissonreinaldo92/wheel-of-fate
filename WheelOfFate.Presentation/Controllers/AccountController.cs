﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WheelOfFate.Presentation.Models;
using Microsoft.AspNet.Identity.Owin;
using WheelOfFate.Presentation.App_Start.IdentityConfig;
using WheelOfFate.Contracts;

namespace WheelOfFate.Presentation.Controllers
{

    public class AccountController : Controller
    {
        public AccountController()
            : this(new WheelOfFateUserManager(new WheelOfFateStore()))
        {
        }

        public AccountController(WheelOfFateUserManager userManager)
        {
            UserManager = userManager;
        }

        public WheelOfFateUserManager UserManager { get; private set; }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var sign = HttpContext.GetOwinContext().Get<WheelOfFateSignInManager>();
                var status = await sign.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, true);
                switch (status)
                {
                    case Microsoft.AspNet.Identity.Owin.SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case Microsoft.AspNet.Identity.Owin.SignInStatus.LockedOut:
                        ModelState.AddModelError("", "You've reached the maximum number of tries. Your account has been locked out for a while. Try again later!!!");
                        break;
                    case Microsoft.AspNet.Identity.Owin.SignInStatus.RequiresVerification:
                        ModelState.AddModelError("", "Requires Verification?? This feature has not been implemented yet...");
                        break;
                    case Microsoft.AspNet.Identity.Owin.SignInStatus.Failure:
                        ModelState.AddModelError("", "Invalid username or password.");
                        break;
                    default:
                        break;
                }
                //isPasswordValid = await UserManager.CheckPasswordAsync(user, model.Password);
                //if (isPasswordValid)
                //{
                //    await SignInAsync(user, model.RememberMe);
                //}
                //}
                //ModelState.AddModelError("", "Invalid username or password.");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

    }
}