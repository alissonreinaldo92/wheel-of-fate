﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WheelOfFate.Presentation.ViewModels;

namespace WheelOfFate.Presentation.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeIndexViewModel { TeamId = "3fc6e49b-38f2-4bcb-b6ef-9d4a5538ba73" };
            return View(model);
        }

        public ActionResult Dashboard()
        {
            ViewData["SubTitle"] = "Simple example of second view";
            ViewData["Message"] = "Data are passing to view by ViewData from controller";

            return View();
        }
    }
}